export interface FormatJson {
    id: string;
    format: string;
    formatDisplay: string;
    type: number;
}