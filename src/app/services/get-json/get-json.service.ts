import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetJsonService {

  constructor(private http: HttpClient) { }

  getJsonWindow() {
    return this.http.get('assets/window.json');
  }

  getJsonFormats() {
    return this.http.get('assets/formats.json');
  }
}
