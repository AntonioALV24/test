import { Component, OnInit } from '@angular/core';
import { GetJsonService } from 'src/app/services/get-json/get-json.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FormatJson } from 'src/app/models/format.model';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.css']
})
export class CustomSelectComponent implements OnInit {
  selectStyle: SafeStyle;
  selectOptions: FormatJson[];

  selectedOption: FormatJson;

  formatForm: FormGroup;


  constructor(
    private getJson: GetJsonService,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
  ) { 
    this.getJsonWindow();
    this.getJsonFormats();
  }

  ngOnInit() {
    this.formatForm = this.fb.group({
      id: new FormControl({value: '', disabled: true}),
      format: new FormControl({value: '', disabled: true}),
      formatDisplay: new FormControl({value: '', disabled: false}),
      type: new FormControl({value: '', disabled: true}),
    })
  }

  private getJsonWindow() {
    this.getJson.getJsonWindow().subscribe((res: any) => {
      let selectControl = res.controls.find(c => c.type === 'select');
      this.selectStyle = this.sanitizer.bypassSecurityTrustStyle(selectControl.attributes.style);
    });
  }

  private getJsonFormats() {
    this.getJson.getJsonFormats().subscribe((res: FormatJson[]) => {
      this.selectOptions = res.filter(f => f.type === 1);
    });
  }

  public selectValue(event: any) {
    this.formatForm.reset();
    this.selectedOption = this.selectOptions.find((o: FormatJson) => o.id === event.target.value)
    this.sendValues();
  }

  public save(){
    if(this.formatForm.valid) {
      let newValue = this.formatForm.get('formatDisplay').value;
      let i = this.selectOptions.findIndex(o => o.id === this.selectedOption.id);
      this.selectOptions[i].formatDisplay = newValue;
      this.formatForm.reset();
      this.sendValues();
      alert('Se capturo el valor correctamente');
    }
  }

  sendValues() {
    this.formatForm.get('id').setValue(this.selectedOption.id);
    this.formatForm.get('format').setValue(this.selectedOption.format);
    this.formatForm.get('formatDisplay').setValue(this.selectedOption.formatDisplay);
    this.formatForm.get('type').setValue(this.selectedOption.type);
  }

}
